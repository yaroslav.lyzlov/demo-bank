import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {
  constructor(private http: HttpClient) {}

  createTransaction(text: string) {
    return this.http.post(environment.apiUrl + 'transactions', {
      transaction_data: {
        text,
        text_render_type: 'markdown'
      },
      notification_type: 'Push',
      callback_url: 'http://abs.absnet.local/transaction_callback',
      confirm_code_length: 8,
      ttl: 0,
      extended_check: false,
      autosing_enabled: false
    });
  }

  getTransaction(id): Observable<any> {
    return this.http.get(environment.apiUrl + 'transactions/' + id);
  }
}
